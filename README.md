# Models legal

## Install
* Software needed:
```sh
python 3.6.7
pip 9.0.1
```
Some models can be run on an nvidia GPU if proper drivers are installed. The same running parameters are used for GPU/CPU.
For CPU no drivers are needed.

* Install python packages needed:
```sh
pip install -r requirements.txt 
```

## Run models
You need to download *codeDuTravail.json*  and *word2vec_fr_legi.model* from [next cloud](https://nextcloud.readerbench.com/index.php/apps/files/?dir=/Legal&fileid=105693#editor) in the root folder of the project, without changing their names.

* There is a simple API for running the models. The API supports the following parameters:
```sh
    --classes_2          # if set, 0 and 2 classes are merged with 1 class (default not set)
    --model              # supports bayes, svm, lstm or gru (default bayes)
    --category           # if to use category (L, M or D) as a features for rnn mdoels (lstm, gru) (default false)
    --layers             # number of layers for RNN models (lstm, gru) (default 1)
    --cell               # cell size of the RNN models (lstm, gru) (default 256)
    --dense              # dense layer size of the RNN models (lstm, gru) (should be lower than cell size) (default 128)
    --dropout            # droput for the RNN models (lstm, gru) (default 0.2)
    --epochs             # number of train epochs for the RNN models (lstm, gru) (default 7)
    --only_content_words # if to use only content words for sklearn models (bayes, svm)
```
* Example usage:
```sh
    python3 models.py --classes_2 --model lstm --category --layers 1 --cell 128 --dense 64 --dropout 0.2 --ecpohs 5 
```
## Results 
# 2 classes
    - batch size 32
    - sequence length = 400
    - legal embeddings = 300 dimensions
    - epochs 7
    - no stop words
    - only lemmas of words

| model         | use category | layers rnn  | cell size    |dense layer size|dev-acc| dev-f1score-classes (0/1) | test - loss|test-acc | test - f1score -classes (-1/1/2/0)|
| -------------:| ------------:| -----------:| ------------:| --------------:|------:|--------------------------:| ----------:| -------:| ---------------------------------:|
| RNN - GRU     | no           | 1           | 128          | 64             | .91   | .948/.720                 | 0.0491     |.93      |.957/.743                          |
| RNN - GRU     | no           | 1           | 256          | 128            | .93   | .959/.752                 | 0.0527     |.93      |.957/.757                          |
| RNN - LSTM    | no           | 1           | 256          | 128            | .92   | .951/.705                 | 0.0380     |.92      |.950/.727                          |   
| RNN - GRU     | yes          | 1           | 128          | 64             | .92   | .955/.730                 | 0.0379     |.92      |.951/.704                          |
| RNN - GRU     | yes          | 1           | 256          | 128            | .93   | .956/.752                 | 0.0381     |.92      |.956/.745                          |
| RNN - LSTM    | yes          | 1           | 256          | 128            | .93   | .951/.691                 | 0.0500     |.92      |.947/.707                          |
