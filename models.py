import json
from pprint import pprint

import numpy as np
import os
import re
import spacy

import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, GRU, Input, concatenate
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.callbacks import ModelCheckpoint, LambdaCallback, Callback

from keras.backend.tensorflow_backend import set_session


from gensim.models.keyedvectors import KeyedVectors

from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from sklearn import datasets
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from typing import List

import argparse
args = None

from collections import Counter

small_run = True
# different doc classifiers, lstm, naive bayes, svm
class DocClassifiers:
    # filename is an elasticsearch dump, use npm elasticdump
    MAX_NB_WORDS = 1000000
    SPACY_FR_MODEL = 'fr_core_news_sm'

    def __init__(self):
        pass

    # elasticsearch dump file
    def load_data(self, filename='codeDuTravail.json'):
        global args
        texts, labels, articles_type = [], [], []
        map_article_types = {'R': 0, 'L': 1, 'D': 2}
        labels_key, content_key, article_type_key = 'isRelevant', 'Content', 'Title'
        cnt_skipped_examples = 0

        with open(filename, 'r', encoding='utf-8', errors='replace') as f:
            index = 0
            for line in f:
                index += 1
                # skip first x examples
                if index > 0:
                    data = json.loads(line)
                    try:
                        label = int(data['_source'][labels_key])
                        article_type = map_article_types[data['_source'][article_type_key][0]]
                    except:
                        cnt_skipped_examples += 1
                        continue
                    
                    if args.classes_2 == True:
                        if label == 0 or label == 2:
                            label = 1
                        
                    if small_run == True:
                        if label == 0 or label == 2:
                            continue
                    
                    if len(data['_source'][content_key]) < 10:
                        cnt_skipped_examples += 1
                        continue
                    txt = data['_source'][content_key]
                    labels.append(label)
                    articles_type.append(article_type)
                    texts.append(txt)
            print('dataset loaded, skipped examples {} from total of {}, remaining {}'.format(cnt_skipped_examples, index, len(labels)))
            print('labels distribution, all: ')
            counter_labels = Counter(labels)
            print(counter_labels)
        if small_run == True:
            return texts[:100], labels[:100], articles_type[:100]
        else:
            return texts, labels, articles_type
    # predictions - predicted values
    # correct_values - correct values 
    # class_stats - class for which to show stats
    def compute_class_stats(self, predictions, correct_values, class_stats=1):
        true_positive, true_negative, false_positive, false_negative = 0, 0, 0, 0
        for i, p in enumerate(predictions):
            if p == correct_values[i]:
                if p == class_stats:
                    true_positive += 1
                else:
                    true_negative += 1
            else:
                if p == class_stats:
                    false_positive += 1
                else:
                    false_negative += 1

        if true_positive != 0:
            precision = true_positive / (true_positive + false_positive)
            recall = true_positive / (true_positive + false_negative)
            f1score = precision * recall * 2 / (precision + recall)
        else:
            return 0, 0, 0
        return precision, recall, f1score

    def compute_conf_matrix(self, stats_for_classes, predictions, test_labels):
        conf_matrix = {class_nr: {} for class_nr in stats_for_classes}
        for c in conf_matrix:
            for class_nr in stats_for_classes:
                conf_matrix[c][class_nr] = 0

        for i in range(len(predictions)):
            conf_matrix[test_labels[i]][predictions[i]] += 1
        return conf_matrix
    
    def print_conf_matrix(self, conf_matrix, dataset='test'):
        print('conf matrix - {} dataset: '.format(dataset))
        for correct in conf_matrix:
            print('values for {}'.format(correct))
            print(conf_matrix[correct])
           
    # eliminate stop words, keep lemmas form of tokens, only content words, for sklearn models
    def spacy_tokenizer_fr(self, text):
        global args
        doc = self.nlp_fr(text)
        doc = filter(lambda token: not token.is_stop, doc)
        if args.only_content_words == True:
            doc = filter(lambda token: token.pos_ == "NOUN" or token.pos_ == "VERB"\
                or token.pos_ == "ADP" or token.pos_ == "ADJ" or token.pos_ == "ADV", doc)
        return [token.lemma_ for token in doc]
    
    def make_categorical(self, labels: List[int], nr_classes: int) -> List[List[int]]:
        global args
        res = np.zeros((len(labels), nr_classes), dtype=np.float32)
        for i, label in enumerate(labels):
            if args.classes_2 == False:
                if label < 0:
                    res[i,nr_classes + label] = 1.0
                else:
                    res[i,label] = 1.0
            else:
                if label < 0:
                    res[i, 0] = 1.0
                else:
                    res[i, label] = 1.0
        return res

    # take only lemmas, preprocess for neural models
    def preprocess_texts_nn(self, all_texts: List[str]) -> List[List[int]]:
        self.nlp_fr = spacy.load(DocClassifiers.SPACY_FR_MODEL)
        docs = [self.nlp_fr(text) for text in all_texts]
        lemmas = set([token.lemma_ for doc in docs for token in doc])
        word_index = dict((token, i) for i, token in enumerate(lemmas))
        #int_to_token = dict((i, token) for i, token in enumerate(lemmas))
        sequences = []
        for doc in docs:
            seq = []
            for token in doc:
                seq.append(word_index[token.lemma_])
            sequences.append(np.int32(seq))
        return np.asarray(sequences), word_index

    # split dataset into train: 70%, dev: 15%, test: 15%, balanced classes
    def split_dataset_train_valid_test(self, train=0.7, dev=0.15, test=0.15):
        all_texts, all_labels, _ = self.load_data()
        sss_train_test = StratifiedShuffleSplit(n_splits=1, test_size=(dev+test), random_state=0)
        train_indices, test_indices = list(sss_train_test.split(all_labels, all_labels))[0]
        train_labels, test_labels = [all_labels[i] for i in train_indices] , [all_labels[i] for i in test_indices]

        sss_dev_test = StratifiedShuffleSplit(n_splits=1, test_size=test/(dev + test), random_state=0)
        dev_indices, test_test_indices = list(sss_dev_test.split(test_labels, test_labels))[0]
        dev_indices = [test_indices[i] for i in dev_indices]
        test_test_indices = [test_indices[i] for i in test_test_indices]
        dev_labels, test_test_labels = [all_labels[i] for i in dev_indices], [all_labels[i] for i in test_test_indices]
        print('Train labels distribution: ')
        print(Counter(train_labels))
        print('Dev labels distribution: ')
        print(Counter(dev_labels))
        print('Test labels distribution: ')
        print(Counter(test_test_labels))
        return train_indices, dev_indices, test_indices

    # stats_for_classes -> if None show stats (recall, precision) for all classes
    # model_name is just for printing purposes
    # verbose - stats for each fold 
    # model - sklearn model, must support model.fit(X, y)
    # k_fold cross validation with test is used
    # same_data_split=true ensures that is the same data split every time the function is called, also the same as lstm split
    def run_sklearn_model(self, stats_for_classes=None,\
        model=MultinomialNB(), model_name='multinomial naive bayes', verbose=False, same_data_split=True,\
        k_fold=3, test_split=0.1, do_test=True):
        global args

        all_texts, all_labels, _ = self.load_data()
        if small_run == True:
            if args.classes_2 == False:
                all_labels = [0 if label == -1 else label for label in all_labels]

        if stats_for_classes is None:
            stats_for_classes = [classs for classs in Counter(all_labels)]
        
        self.nlp_fr = spacy.load(DocClassifiers.SPACY_FR_MODEL)
        print('running tf-idf...')
        tfidf = TfidfVectorizer(sublinear_tf=True, min_df=6, norm='l2', encoding='utf-8', ngram_range=(1, 1),
                                analyzer='word',\
                                stop_words=None,\
                                tokenizer=self.spacy_tokenizer_fr)
        # tf_idf_features has shape (nr_docs, nr_features), where nr_features is the number of words in all docs
        tf_idf_features = tfidf.fit_transform(all_texts).toarray()
        print('done tf-idf')
        print('splitting dataset...')
        # split the dataset into test and fit dataset, fit dataset is further split into train/dev with k-fold cross valid
        random_state = 0 if same_data_split == True else None
        sss = StratifiedShuffleSplit(n_splits=1, test_size=test_split, random_state=random_state)
        fit_indices, test_indices = list(sss.split(np.zeros(len(all_labels)), all_labels))[0]
        fit_texts, test_texts = [all_texts[i] for i in fit_indices] , [all_texts[i] for i in test_indices]
        
        fit_labels, test_labels = [all_labels[i] for i in fit_indices] , [all_labels[i] for i in test_indices]
        # show stats of splits
        print('labels distribution, fit: ')
        print(Counter(fit_labels))
        print('labels distribution, test: ')
        print(Counter(test_labels))

        dev_precision, dev_recall, dev_f1_score = {}, {}, {}
        conf_matrices, accuracy_dev = [], 0
        for class_stats in stats_for_classes:
            dev_precision[class_stats], dev_recall[class_stats], dev_f1_score[class_stats] = 0, 0, 0
        skf = StratifiedKFold(n_splits=k_fold, random_state=random_state, shuffle=False)
        skf_splits = skf.split(np.zeros(len(fit_labels)), fit_labels)
        k_fold_index = 0

        # fit the model, iterate through each fold
        for train_indices_fit, dev_indices_fit in list(skf_splits):
            k_fold_index += 1
            # dev/train_indices_fit are indices relative to the fit dataset, not all dataset which we need
            # dev/train_indices_all are indices relative to all dataset
            train_indices_all, dev_indices_all = [fit_indices[i] for i in train_indices_fit], [fit_indices[i] for i in dev_indices_fit]

            _, train_labels = [all_texts[i] for i in train_indices_all], [all_labels[i] for i in train_indices_all]
            _, dev_labels = [all_texts[i] for i in dev_indices_all], [all_labels[i] for i in dev_indices_all]
            if verbose == True:
                print('{}-fold labels distribution, train: '.format(k_fold_index))
                print(Counter(train_labels))
                print('{}-fold labels distribution, dev: '.format(k_fold_index))
                print(Counter(dev_labels))

            # running the model for one fold
            print('{0}-fold running {1}... '.format(k_fold_index, model_name))
            features_train = [tf_idf_features[i] for i in train_indices_all]
            features_dev = [tf_idf_features[i] for i in dev_indices_all]
            
            model.fit(features_train, train_labels)
            predictions = model.predict(features_dev)
            acc = 0
            for pred, label in zip(predictions, dev_labels):
                if pred == label:
                    acc += 1
            acc /= len(predictions)
            accuracy_dev += acc
            conf_matrix = self.compute_conf_matrix(stats_for_classes, predictions, dev_labels)
            conf_matrices.append(conf_matrix)

            for class_stats in stats_for_classes:
                precision, recall, f1score = self.compute_class_stats(predictions, dev_labels, class_stats=class_stats)
                dev_precision[class_stats] += precision
                dev_recall[class_stats] += recall
                dev_f1_score[class_stats] += f1score

        print('{0} - dev dataset - accuracy {1:.2f}'.format(model_name, accuracy_dev/k_fold))
        for class_stats in stats_for_classes:
            dev_precision[class_stats] /= k_fold
            dev_recall[class_stats] /= k_fold
            dev_f1_score[class_stats] /= k_fold
            print('{4} - dev dataset - class {3:>2} | precision: {0:.3f} recall: {1:.3f} f1score: {2:.3f}'\
                    .format(dev_precision[class_stats], dev_recall[class_stats], dev_f1_score[class_stats], class_stats, model_name))
        
        for matrix in conf_matrices[1:]:
            for correct_class in matrix:
                for pred_class in matrix[correct_class]:
                    conf_matrices[0][correct_class][pred_class] += 1
        self.print_conf_matrix(conf_matrices[0], dataset='dev')

        # test model
        if do_test == True:
            features_fit = [tf_idf_features[i] for i in fit_indices]
            features_test = [tf_idf_features[i] for i in test_indices]
            model.fit(features_fit, fit_labels)
            predictions = model.predict(features_test)
            acc = 0
            for pred, label in zip(predictions, dev_labels):
                if pred == label:
                    acc += 1
            acc /= len(predictions)

            print('{0} - test dataset - accuracy {1:.2f}'.format(model_name, acc))
            for class_stats in stats_for_classes:
                precision, recall, f1score = self.compute_class_stats(predictions, test_labels, class_stats=class_stats)
                print('{4} - test dataset - class {3:>2} | precision: {0:.3f} recall: {1:.3f} f1score: {2:.3f}'\
                 .format(precision, recall, f1score, class_stats, model_name))
            
            conf_matrix = self.compute_conf_matrix(stats_for_classes, predictions, test_labels)
            self.print_conf_matrix(conf_matrix, dataset='test')


    def construct_rnn_model(self, word_index, embedding_matrix, nr_classes, layers=2,\
        dropout=0.3, batch_size=32, epochs=5, cell=256, max_seq_length=3000, embeddings_dim=300):
        global args
        # if no category -> simple seq model (input output)
        # if args.use_category == False:
        #     model = Sequential()
        #     model.add(Embedding(len(word_index) + 1,
        #                         embeddings_dim,
        #                         weights=[embedding_matrix],
        #                         input_length=max_seq_length,
        #                         trainable=False))
        #     for _ in range(layers - 1):
        #         if args.model == "lstm":
        #             model.add(Bidirectional(LSTM(cell, return_sequences=True)))
        #         else:
        #             model.add(Bidirectional(GRU(cell, return_sequences=True)))
        #         model.add(Dropout(dropout))
        #     if args.model == "lstm":
        #         model.add(Bidirectional(LSTM(cell, return_sequences=False)))
        #     else:
        #         model.add(Bidirectional(GRU(cell, return_sequences=False)))
            
        #     model.add(Dropout(dropout))
        #     model.add(Dense(args.dense, activation='tanh'))
        #     model.add(Dropout(dropout))
        #     model.add(Dense(nr_classes, activation='softmax'))
        #     model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])
        # else:
        input_seq_text = Input(shape=(max_seq_length,))
        embeddings = Embedding(len(word_index) + 1,\
                            embeddings_dim,\
                            weights=[embedding_matrix],\
                            input_length=max_seq_length,\
                            trainable=False)(input_seq_text)
        if args.model == 'lstm':
            rnn = LSTM(units=cell, input_shape=(max_seq_length, embeddings_dim,), return_sequences=False)
        else:
            rnn = GRU(units=cell, input_shape=(max_seq_length, embeddings_dim,), return_sequences=False)
        bi_rnn = Bidirectional(layer=rnn, merge_mode='concat')(embeddings)

        # add other features (article type) by concating them to the rnn output
        if args.category == True:
            article_type_input = Input(shape=(1,))
            merged_layer = concatenate([bi_rnn, article_type_input], axis=-1)
        else:
            merged_layer = bi_rnn

        dense = Dense(args.dense, activation='tanh')(merged_layer)
        drop = Dropout(dropout)(dense)
        output = Dense(nr_classes, activation='softmax')(drop)
        if args.category == True:
            model = Model(inputs=[input_seq_text, article_type_input], outputs=output)
        else:
            model = Model(inputs=input_seq_text, outputs=output)
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        return model

    # k_fold cross validation with test is used
    def run_rnn_model(self, model_name='lstm', word_embeddings_model='word2vec_fr_legi.model',\
             do_test=True, same_data_split=True, k_fold=3, test_split=0.1,\
             stats_for_classes=None,\
             layers=2, dropout=0.3, batch_size=32, epochs=13, cell=256, max_seq_length=400, embeddings_dim=300,\
             max_nb_words=100000):
        global small_run
        if small_run == False:
            config = tf.ConfigProto()
            #config.gpu_options.per_process_gpu_memory_fraction = 0.2
            config.gpu_options.allow_growth = True
            set_session(tf.Session(config=config))
        all_texts, all_labels, articles_type = self.load_data()
        if small_run == True:
            if args.classes_2 == False:
                all_labels = [0 if label == -1 else label for label in all_labels]

        nr_classes = len(Counter(all_labels))

        if stats_for_classes is None:
            stats_for_classes = [classs for classs in Counter(all_labels)]

        sequences, word_index = self.preprocess_texts_nn(all_texts)
        data = pad_sequences(sequences, maxlen=max_seq_length)

        print('Found %s unique tokens.' % len(word_index))
        print('Max seq is {}'.format(max(len(inst) for inst in sequences)))
        random_state = 0 if same_data_split == True else None
        sss = StratifiedShuffleSplit(n_splits=1, test_size=test_split, random_state=random_state)
        fit_indices, test_indices = list(sss.split(np.zeros(len(all_labels)), all_labels))[0]
        fit_texts, test_texts = [all_texts[i] for i in fit_indices] , [all_texts[i] for i in test_indices]
        fit_articles_type, test_articles_type = [articles_type[i] for i in fit_indices] , [articles_type[i] for i in test_indices]
        fit_labels, test_labels = [all_labels[i] for i in fit_indices] , [all_labels[i] for i in test_indices]
        
        # show stats of splits
        print('labels distribution, fit: ')
        print(Counter(fit_labels))
        print('labels distribution, test: ')
        print(Counter(test_labels))

        dev_precision, dev_recall, dev_f1_score = {}, {}, {}
        conf_matrices, accuracy_dev = [], 0
        for class_stats in stats_for_classes:
            dev_precision[class_stats], dev_recall[class_stats], dev_f1_score[class_stats] = 0, 0, 0
        skf = StratifiedKFold(n_splits=k_fold, random_state=random_state, shuffle=False)
        skf_splits = skf.split(np.zeros(len(fit_labels)), fit_labels)
        k_fold_index = 0

        # model embeddings 
        word_emb = KeyedVectors.load_word2vec_format(word_embeddings_model, binary=False)
        embedding_matrix = np.zeros((len(word_index) + 1, embeddings_dim))
        for word, i in word_index.items():
            embedding_vector = word_emb[word] if word in word_emb  else np.zeros(embeddings_dim)
            embedding_matrix[i] = embedding_vector
        dev_precision, dev_recall, dev_f1_score = {}, {}, {}
        for class_stats in stats_for_classes:
            dev_precision[class_stats], dev_recall[class_stats], dev_f1_score[class_stats] = 0, 0, 0

        # fit the model, iterate through each fold
        for train_indices_fit, dev_indices_fit in list(skf_splits):
            k_fold_index += 1
            # dev/train_indices_fit are indices relative to the fit dataset, not all dataset which we need
            # dev/train_indices_all are indices relative to all dataset
            train_indices_all, dev_indices_all = [fit_indices[i] for i in train_indices_fit], [fit_indices[i] for i in dev_indices_fit]

            _, train_labels = [all_texts[i] for i in train_indices_all], [all_labels[i] for i in train_indices_all]
            _, dev_labels = [all_texts[i] for i in dev_indices_all], [all_labels[i] for i in dev_indices_all]
            print('dev distribution: ')
            print(Counter(dev_labels))
            # construct input tensor
            x_train_text = np.asarray([data[i] for i in train_indices_all])
            x_train_article_types = np.asarray([articles_type[i] for i in train_indices_all])
            y_train = self.make_categorical(train_labels, nr_classes)

            x_val_text = np.asarray([data[i] for i in dev_indices_all])
            x_val_article_types = np.asarray([articles_type[i] for i in dev_indices_all])
            y_val = self.make_categorical(dev_labels, nr_classes)

            # construct model
            model = self.construct_rnn_model(word_index, embedding_matrix, nr_classes,\
                layers=layers, dropout=dropout, batch_size=batch_size, epochs=epochs, cell=cell, max_seq_length=max_seq_length,\
                embeddings_dim=embeddings_dim)

            print('Train...')
            if args.category == True:
                model.fit([x_train_text, x_train_article_types], y_train,
                        batch_size=batch_size,
                        epochs=epochs)
                predictions = model.predict([x_val_text, x_val_article_types], batch_size=1)
            else:
                model.fit(x_train_text, y_train,
                        batch_size=batch_size,
                        epochs=epochs)
                predictions = model.predict(x_val_text, batch_size=1)
            
            if small_run == False:
                if args.classes_2 == False:
                    predictions = [-1 if np.argmax(p_vec) == (nr_classes - 1) else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == (nr_classes - 1) else np.argmax(y) for y in y_val]
                else:
                    predictions = [-1 if np.argmax(p_vec) == 0 else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == 0 else np.argmax(y) for y in y_val]
            else:
                if args.classes_2 == False:
                    predictions = [np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [np.argmax(y) for y in y_val]
                else:
                    predictions = [-1 if np.argmax(p_vec) == 0 else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == 0 else np.argmax(y) for y in y_val]
            acc = 0
            for pred, label in zip(predictions, correct_labels_dev):
                if pred == label:
                    acc += 1
            acc /= len(predictions)
            accuracy_dev += acc
            conf_matrices.append(self.compute_conf_matrix(stats_for_classes, predictions, correct_labels_dev))

            for class_stats in stats_for_classes:
                precision, recall, f1score = self.compute_class_stats(predictions, correct_labels_dev, class_stats=class_stats)
                dev_precision[class_stats] += precision
                dev_recall[class_stats] += recall
                dev_f1_score[class_stats] += f1score

        print('{0} - dev dataset - accuracy {1:.2f}'.format(model_name, accuracy_dev/k_fold))
        for class_stats in stats_for_classes:
            dev_precision[class_stats] /= k_fold
            dev_recall[class_stats] /= k_fold
            dev_f1_score[class_stats] /= k_fold
            print('{4} - dev dataset - class {3:>2} | precision: {0:.3f} recall: {1:.3f} f1score: {2:.3f}'\
                    .format(dev_precision[class_stats], dev_recall[class_stats], dev_f1_score[class_stats], class_stats, model_name))
        
        for matrix in conf_matrices[1:]:
            for correct_class in matrix:
                for pred_class in matrix[correct_class]:
                    conf_matrices[0][correct_class][pred_class] += 1
        self.print_conf_matrix(conf_matrices[0], dataset='dev')

        # test
        if do_test == True:
            # now train on all fit dataset, test on test
            x_final_train_text = np.asarray([data[i] for i in fit_indices])
            x_final_train_articles_types = np.asarray([articles_type[i] for i in fit_indices])
            y_final_train = self.make_categorical(fit_labels, nr_classes)

            x_test_text = np.asarray([data[i] for i in test_indices])
            x_test_articles_types = np.asarray([articles_type[i] for i in test_indices])
            y_test = self.make_categorical(test_labels, nr_classes)

            # construct model
            model = self.construct_rnn_model(word_index, embedding_matrix, nr_classes,\
                dropout=dropout, batch_size=batch_size, epochs=epochs, cell=cell, max_seq_length=max_seq_length,\
                embeddings_dim=embeddings_dim)
            print(model.summary())
            # final train
            print('Final train...')
            filepath_save = "models/" +  model_name + "-{epoch:02d}-{loss:.2f}.hdf5"
            checkpoint = ModelCheckpoint(filepath_save, monitor='loss', verbose=1, save_best_only=True, mode='min')
            callbacks_list = [checkpoint]
            if args.category == True:
                model.fit([x_final_train_text, x_final_train_articles_types], y_final_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        callbacks=callbacks_list)
                predictions = model.predict([x_test_text, x_test_articles_types], batch_size=1)
            else:
                model.fit(x_final_train_text, y_final_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        callbacks=callbacks_list)
                predictions = model.predict(x_test_text, batch_size=1)

            if small_run == False:
                if args.classes_2 == False:
                    predictions = [-1 if np.argmax(p_vec) == (nr_classes - 1) else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == (nr_classes - 1) else np.argmax(y) for y in y_test]
                else:
                    predictions = [-1 if np.argmax(p_vec) == 0 else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == 0 else np.argmax(y) for y in y_test]
            else:
                if args.classes_2 == False:
                    predictions = [np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [np.argmax(y) for y in y_test]
                else:
                    predictions = [-1 if np.argmax(p_vec) == 0 else np.argmax(p_vec) for p_vec in predictions]
                    correct_labels_dev = [-1 if np.argmax(y) == 0 else np.argmax(y) for y in y_test]
            acc = 0
            for pred, label in zip(predictions, correct_labels_dev):
                if pred == label:
                    acc += 1
            acc /= len(predictions)
            print('{0} - test dataset - accuracy {1:.2f}'.format(model_name, acc))
            for class_stats in stats_for_classes:
                precision, recall, f1score = self.compute_class_stats(predictions, correct_labels_dev,\
                    class_stats=class_stats)
                print('{4} - test dataset - class {3:>2} | precision: {0:.3f} recall: {1:.3f} f1score: {2:.3f}'\
                .format(precision, recall, f1score, class_stats, model_name))

            conf_matrix = self.compute_conf_matrix(stats_for_classes, predictions, correct_labels_dev)
            self.print_conf_matrix(conf_matrix, dataset='test')
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--classes_2', dest='classes_2', action='store_true', default=False)
    parser.add_argument('--model', dest='model', action='store', default='lstm')
    # use category of the law
    parser.add_argument('--category', dest='category', action='store_true', default=False) # does not work for sklearn models
    parser.add_argument('--layers', dest='layers', action='store', default=2, type=int)
    parser.add_argument('--cell', dest='cell', action='store', default=256, type=int)
    parser.add_argument('--dense', dest='dense', action='store', default=128, type=int)
    parser.add_argument('--dropout', dest='dropout', action='store', default=0.2, type=float)
    parser.add_argument('--epochs', dest='epochs', action='store', default=7, type=int)
    parser.add_argument('--only_content_words', dest='only_content_words', action='store_true', default=False)
    args = parser.parse_args()

    for k in args.__dict__:
        if args.__dict__[k] is not None:
            print(k, '->', args.__dict__[k])

    doc_classifier = DocClassifiers()
    if args.model == "bayes":
        doc_classifier.run_sklearn_model()
    elif args.model == "svm":
        doc_classifier.run_sklearn_model(model=svm.SVC(kernel='linear'), model_name='svm with linear kernel')
    else:
        doc_classifier.run_rnn_model(layers=args.layers, epochs=args.epochs,\
             cell=args.cell, dropout=args.dropout, max_seq_length=400)
    #     model_name='lstm_256_drop_02_batch_32_seq_400', max_seq_length=100)
    #doc_classifier.split_dataset_train_valid_test()

    

